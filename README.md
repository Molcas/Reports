Special project to run some auxiliary jobs fetching data from [OpenMolcas](https://gitlab.com/Molcas/OpenMolcas) pipelines.

At the moment this does the following:

* Get a list of all the untested keywords in the latest merged pipeline: [`report.md`](https://gitlab.com/Molcas/Aux/Reports/-/jobs/artifacts/main/raw/report.md?job=report)

* Collect all the time estimates from the latest merged pipeline, so they can be used for load balancing.
