#!/usr/bin/env python3

import requests
import json
import os
from lxml import etree as ET

def get_url(project):
  return f'https://gitlab.com/api/v4/projects/{project}'

project = os.environ.get('REPORTS_PROJECT', 'Molcas/OpenMolcas')
branch = os.environ.get('REPORTS_BRANCH', 'master')

url = get_url(project.replace('/', '%2F'))

# First get the SHA of the current master
link = f'{url}/repository/branches/{branch}'
r = requests.get(link)
master_sha = r.json()['commit']['id']


# ===== Get keyword.xml =====

# Find the latest master pipeline
latest_pl = None
link = f'{url}/pipelines?ref={branch}'
page=0
while link:
  page += 1
  r = requests.get(link)
  pl_list = r.json()
  for pl in pl_list:
    if pl['sha'] == master_sha and pl['status'] == 'success':
      latest_pl = pl['id']
      break
  if latest_pl:
    break
  try:
    link = r.links['next']['url']
  except KeyError:
    link = None

# Get all jobs in the pipeline (there should only be a few)
link = f'{url}/pipelines/{latest_pl}/jobs?per_page=50'
r = requests.get(link)
job_list = r.json()

# Identify the doc job
doc_job = None
for j in job_list:
  if j['name'] == 'doc' and j['status'] == 'success':
    doc_job = j['id']

# From the doc job, get the keyword.xml file
link = f'{url}/jobs/{doc_job}/artifacts/install_dir/data/keyword.xml'
r = requests.get(link)
kxml = ET.fromstring(r.text)


# ===== Get the list of test jobs =====

# Find the latest MR (i.e. matching the master SHA)
latest_MR = None
link = f'{url}/merge_requests/?state=merged&target_branch={branch}'
page=0
while link:
  page += 1
  r = requests.get(link)
  MR_list = r.json()
  for MR in MR_list:
    if MR['sha'] == master_sha:
      latest_MR = MR['iid']
      break
  if latest_MR:
    break
  try:
    link = r.links['next']['url']
  except KeyError:
    link = None

# Find the latest succesful pipeline for the MR
link = f'{url}/merge_requests/{latest_MR}/pipelines'
r = requests.get(link)
for pl in r.json():
  if pl['sha'] == master_sha and pl['status'] == 'success':
    pipeline = pl['id']
    break

# Get all jobs in the pipeline
job_list = []
link = f'{url}/pipelines/{pipeline}/jobs?per_page=50'
r = requests.get(link)
page=0
while link:
  page += 1
  r = requests.get(link)
  job_list.extend(r.json())
  try:
    link = r.links['next']['url']
  except KeyError:
    link = None

# Get jobs from downstream pipelines
outsource = None
run_jobs = None
link = f'{url}/pipelines/{pipeline}/bridges'
r = requests.get(link)
bridge_list = r.json()
for b in bridge_list:
  if b['name'] == 'outsource' and b['downstream_pipeline']['status'] == 'success':
    outsource = b['downstream_pipeline']
    break
if outsource:
  url_ = get_url(outsource["project_id"])
  link = f'{url_}/pipelines/{outsource["id"]}/bridges'
  r = requests.get(link)
  bridge_list = r.json()
  for b in bridge_list:
    if b['name'] == 'run_jobs' and b['downstream_pipeline']['status'] == 'success':
      run_jobs = b['downstream_pipeline']
      break
if run_jobs:
  url_ = get_url(run_jobs["project_id"])
  link = f'{url_}/pipelines/{run_jobs["id"]}/jobs?per_page=50'
  r = requests.get(link)
  page=0
  while link:
    page += 1
    r = requests.get(link)
    job_list.extend(r.json())
    try:
      link = r.links['next']['url']
    except KeyError:
      link = None

# Identify the test jobs
test_list = {}
for j in job_list:
  if j['stage'] == 'test' and j['status'] == 'success':
    name = j['name'].split()[0].replace(':', '_')
    if not name in test_list:
      test_list[name] = {}
    try:
      num = int(j['name'].split()[1].split('/')[0])
    except:
      num = 0
    test_list[name][num] = j


# ===== Get timing data =====

# For each test type, merge all timing data
for test in test_list:
  failed = False
  filename = f'{test}.timest'
  with open(filename, 'w') as f:
    f.write('trust_skip\n')
    for i in sorted(test_list[test]):
      url_ = get_url(test_list[test][i]['pipeline']['project_id'])
      link = f'{url_}/jobs/{test_list[test][i]["id"]}/artifacts/timest'
      r = requests.get(link)
      if not r.ok:
        failed = True
        break
      for l in r.text.split('\n'):
        if l.startswith('#') or not l:
          continue
        f.write(l + '\n')
  if failed:
    os.remove(filename)


# ===== Get keyword usage data =====

# From the test jobs, merge all the keylist files in a single one
keylist = {}
for test in test_list:
  for i in sorted(test_list[test]):
    url_ = get_url(test_list[test][i]['pipeline']['project_id'])
    link = f'{url_}/jobs/{test_list[test][i]["id"]}/artifacts/keylist'
    r = requests.get(link)
    newlist = json.loads(r.text)
    for mod in newlist:
      if mod in keylist:
        keylist[mod].update(set(newlist[mod]))
      else:
        keylist[mod] = set(newlist[mod])

# Now we have an XML object with the keyword.xml content,
# and a keylist dict with all the tested keywords.

not_tested = {}
for mod in kxml.findall('MODULE'):
  modname = mod.get('NAME')
  if modname == 'ENVIRONMENT':
    continue
  if modname in keylist:
    kwlist = []
    for kw in mod.findall('.//KEYWORD'):
      kwname = kw.get('NAME')
      if kwname not in keylist[modname]:
        kwlist.append(kwname)
    if kwlist:
      not_tested[modname] = kwlist
  else:
    not_tested[modname] = None

# Find keywords from included modules and remove from unused list
for mod in kxml.findall('MODULE'):
  modname = mod.get('NAME')
  for inc in mod.findall('.//INCLUDE'):
    incname = inc.get('MODULE')
    exclist = inc.get('EXCEPT')
    exclist = exclist.split(',') if exclist else []
    if incname not in not_tested:
      continue
    incmod = kxml.find(f'MODULE[@NAME="{incname}"]')
    for kw in incmod.findall('.//KEYWORD'):
      kwname = kw.get('NAME')
      if kwname in exclist:
        continue
      if kwname in keylist[modname]:
        if kwname in not_tested[incname]:
          not_tested[incname].remove(kwname)

# Print a report

url = f'https://gitlab.com/{project}'
print(f'Report from pipeline [{pipeline}]({url}/-/pipelines/{pipeline}), commit [{master_sha:.8}]({url}/-/commit/{master_sha})')
print()
print('Untested modules')
print('================')
print()
for mod in sorted(not_tested):
  if not_tested[mod] is None:
    print(f'* `{mod}`')
print()
print('Untested keywords')
print('=================')
print()
for mod in sorted(not_tested):
  if not_tested[mod]:
    print(f'* `{mod}`')
    for kw in sorted(not_tested[mod]):
      print(f'  * `{kw}`')
